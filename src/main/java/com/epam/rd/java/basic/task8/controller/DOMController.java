package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.comparators.FlowerComparatorOne;
import com.epam.rd.java.basic.task8.entity.Flower;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List parseToList() {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		LinkedList<Flower> flowers = new LinkedList<>();
		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(this.xmlFileName));
			Flower flower = null;
			while (reader.hasNext()) {
				XMLEvent nextEvent = reader.nextEvent();
				if (nextEvent.isStartElement()) {
					StartElement startElement = nextEvent.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case "flower":
							flower = new Flower();
							break;
						case "name":
							nextEvent = reader.nextEvent();
							flower.setName(nextEvent.asCharacters().getData());
							break;
						case "soil":
							nextEvent = reader.nextEvent();
							flower.setSoil(nextEvent.asCharacters().getData());
							break;
						case "origin":
							nextEvent = reader.nextEvent();
							flower.setOrigin(nextEvent.asCharacters().getData());
							break;
						case "stemColour":
							nextEvent = reader.nextEvent();
							flower.setStemColour(nextEvent.asCharacters().getData());
							break;
						case "leafColour":
							nextEvent = reader.nextEvent();
							flower.setLeafColour(nextEvent.asCharacters().getData());
							break;
						case "aveLenFlower":
							nextEvent = reader.nextEvent();
							flower.setAveLenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
							Attribute lenMeasure = startElement.getAttributeByName(new QName("measure"));
							if (lenMeasure != null) {
								flower.setLengthMeasure(lenMeasure.getValue());
							}
							break;
						case "tempreture":
							nextEvent = reader.nextEvent();
							flower.setTemperature(Integer.parseInt(nextEvent.asCharacters().getData()));
							Attribute tempMeasure = startElement.getAttributeByName(new QName("measure"));
							if (tempMeasure != null) {
								flower.setTemperatureMeasure(tempMeasure.getValue());
							}
							break;
						case "lighting":
							nextEvent = reader.nextEvent();
							Attribute lightRequiring = startElement.getAttributeByName(new QName("lightRequiring"));
							flower.setLighting(lightRequiring.getValue());
							break;

						case "watering":
							nextEvent = reader.nextEvent();
							flower.setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
							Attribute waterMeasure = startElement.getAttributeByName(new QName("measure"));
							if (waterMeasure != null) {
								flower.setWateringMeasure(waterMeasure.getValue());
							}
							break;
						case "multiplying":
							nextEvent = reader.nextEvent();
							flower.setMultiplying(nextEvent.asCharacters().getData());
							break;
					}

				}
				if (nextEvent.isEndElement()) {
					EndElement endElement = nextEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("flower")) {
						flowers.add(flower);
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return flowers;
	}

	public List<Flower> sortList(List<Flower> list) {
		Comparator<Flower> comparator = new FlowerComparatorOne();
		TreeSet<Flower> flowers = new TreeSet(comparator);
		flowers.addAll(list);
		List<Flower> finalFlowersList = new LinkedList<>();
		for (Flower flower : flowers) {
			finalFlowersList.add(flower);
		}
		return finalFlowersList;
	}

	public void populateXML(List<Flower> list, String path) {
		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter streamWriter = null;
		try (FileWriter fileWriter = new FileWriter(path)) {
			streamWriter = outputFactory.createXMLStreamWriter(fileWriter);
			streamWriter.writeStartDocument("UTF-8", "1.0");
			streamWriter.writeStartElement("flowers");
			streamWriter.writeAttribute("xmlns", "http://www.nure.ua");
			streamWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
//			streamWriter.writeAttribute("xsi:chemaLocation", "http://www.nure.ua input.xsd ");

			for (Flower flower : list) {
				streamWriter.writeStartElement("flower");

				streamWriter.writeStartElement("name");
				streamWriter.writeCharacters(flower.getName());
				streamWriter.writeEndElement();

				streamWriter.writeStartElement("soil");
				streamWriter.writeCharacters(flower.getSoil());
				streamWriter.writeEndElement();

				streamWriter.writeStartElement("origin");
				streamWriter.writeCharacters(flower.getOrigin());
				streamWriter.writeEndElement();

				streamWriter.writeStartElement("visualParameters");

				streamWriter.writeStartElement("stemColour");
				streamWriter.writeCharacters(flower.getStemColour());
				streamWriter.writeEndElement();

				streamWriter.writeStartElement("leafColour");
				streamWriter.writeCharacters(flower.getLeafColour());
				streamWriter.writeEndElement();

				streamWriter.writeStartElement("aveLenFlower");
				streamWriter.writeAttribute("measure",flower.getLengthMeasure());
				streamWriter.writeCharacters(String.valueOf(flower.getAveLenFlower()));
				streamWriter.writeEndElement();

				streamWriter.writeEndElement(); // visualParameters
				streamWriter.writeStartElement("growingTips");

				streamWriter.writeStartElement("tempreture");
				streamWriter.writeAttribute("measure",flower.getTemperatureMeasure());
				streamWriter.writeCharacters(String.valueOf(flower.getTemperature()));
				streamWriter.writeEndElement();

				streamWriter.writeStartElement("lighting");
				streamWriter.writeAttribute("lightRequiring",flower.getLighting());
				streamWriter.writeEndElement();

				streamWriter.writeStartElement("watering");
				streamWriter.writeAttribute("measure",flower.getWateringMeasure());
				streamWriter.writeCharacters(String.valueOf(flower.getWatering()));
				streamWriter.writeEndElement();

				streamWriter.writeEndElement();

				streamWriter.writeStartElement("multiplying");
				streamWriter.writeCharacters(flower.getMultiplying());
				streamWriter.writeEndElement();

				streamWriter.writeEndElement();
			}
			streamWriter.writeEndElement();

		} catch (XMLStreamException | IOException e) {
			e.printStackTrace();
		}
	}

}
