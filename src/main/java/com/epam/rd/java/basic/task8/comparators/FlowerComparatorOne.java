package com.epam.rd.java.basic.task8.comparators;

import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.Comparator;

public class FlowerComparatorOne implements Comparator<Flower> {
    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
