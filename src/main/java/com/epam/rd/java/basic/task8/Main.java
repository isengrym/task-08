package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

import java.util.LinkedList;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		LinkedList<Flower> flowers;

		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		DOMController domController = new DOMController(xmlFileName);
		flowers = (LinkedList<Flower>) domController.parseToList();
		flowers = (LinkedList<Flower>) domController.sortList(flowers);
		String outputXmlFile = "output.dom.xml";
		domController.populateXML(flowers,outputXmlFile);



		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);

		flowers = (LinkedList<Flower>) saxController.parseToList();
		flowers = (LinkedList<Flower>) saxController.sortList(flowers);
		outputXmlFile = "output.sax.xml";
		saxController.populateXML(flowers,outputXmlFile);
		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save

		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		STAXController staxController = new STAXController(xmlFileName);

		flowers = (LinkedList<Flower>) staxController.parseToList();
		flowers = (LinkedList<Flower>) staxController.sortList(flowers);
		outputXmlFile = "output.stax.xml";
		staxController.populateXML(flowers,outputXmlFile);
	}

}
