package com.epam.rd.java.basic.task8.entity;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;
    private int temperature;
    private String lighting;
    private int watering;
    private String lengthMeasure;
    private String temperatureMeasure;
    private String wateringMeasure;
    private String multiplying;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public int getWatering() {
        return watering;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public String getLengthMeasure() {
        return lengthMeasure;
    }

    public void setLengthMeasure(String lengthMeasure) {
        this.lengthMeasure = lengthMeasure;
    }

    public String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public void setTemperatureMeasure(String temperatureMeasure) {
        this.temperatureMeasure = temperatureMeasure;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                ", temperature=" + temperature +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                ", lengthMeasure='" + lengthMeasure + '\'' +
                ", temperatureMeasure='" + temperatureMeasure + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                '}';
    }
}
